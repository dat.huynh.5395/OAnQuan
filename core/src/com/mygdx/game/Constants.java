package com.mygdx.game;

/**
 * Created by d40 on 5/8/17.
 */

public class Constants {
    public static final float SCREEN_WIDTH = 800;
    public static final float SCREEN_HEIGHT = 480;
    public static final int ROCK_SIZE = 15;
    public static final int CELL_SIZE = 50;
    public static final int PER_INCREASE = 2;
    public static final class Screen{
        public static final int SPLASH = 0;
        public static final int GAME_SCREEN = 1;
    }
    public static final int TOUCHABLE_WIDTH = 250;
    public static final int TOUCHABLE_HEIGHT = 100;

    public static boolean isUse = false;
    public static int rockLeft = 0;
}
