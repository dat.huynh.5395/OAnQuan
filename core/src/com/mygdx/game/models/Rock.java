package com.mygdx.game.models;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.Constants;
import com.mygdx.game.screens.ScreenManager;

/**
 * Created by kienvanba on 5/9/17.
 */

public class Rock {
    private Vector3 destination;
    private Vector3 position;
    private Vector3 velocity;
    private Texture texture;
    private boolean isKing;
    private int size;
    private Sound coin;
    private float originalDistance;

    public Rock(Vector3 position, boolean isKing){
        this.position = position;
        this.isKing = isKing;
        velocity = new Vector3(0, 0, 0);
        texture = ScreenManager.getInstance().getAssetManager().get(isKing?"green_ball.png":"red_ball.png", Texture.class);
        size = isKing?25:10;
        coin = ScreenManager.getInstance().getAssetManager().get("sound1.ogg", Sound.class);
    }

    public Texture getTexture(){
        return this.texture;
    }

    public Vector3 getPosition(){
        return this.position;
    }

    public int getSize(){
        return this.size;
    }

    public boolean isKing(){
        return this.isKing;
    }

    public void update(float dt){
        if(destination!=null) {
//            if(originalDistance == destination.x-position.x)
//                coin.play();
            if (!near(position, destination, 5)) {
                velocity.set((destination.x - position.x) * 5, (destination.y - position.y) * 5, 0);
                velocity.scl(dt);
                position.add(velocity.x, velocity.y, 0);
                if(near(position, destination, 5)){
                    if (Constants.rockLeft > 0) {
                        Constants.rockLeft--;
                    }
                    if (Constants.rockLeft <= 0) {
                        Constants.isUse = false;
                    }
                }
            }
        }
    }

    private boolean near(Vector3 p1, Vector3 p2, int range){
        return (Math.abs(p2.x-p1.x)<=range && Math.abs(p2.y-p1.y)<=range);
    }

    public void move(Vector3 destination){
        this.destination = destination;
        this.originalDistance = destination.x - position.x;
    }
    public void dispose(){
        texture.dispose();
        coin.dispose();
    }
}
