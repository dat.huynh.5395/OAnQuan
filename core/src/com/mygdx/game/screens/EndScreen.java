package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.Constants;
import com.mygdx.game.models.Turn;

/**
 * Created by kienvanba on 5/17/17.
 */

public class EndScreen extends Screen{
    private Texture backGround;
    private Texture mainTexture;
    private Rectangle replay, exit;

    public EndScreen(){
        camera.setToOrtho(false, Constants.SCREEN_WIDTH/2, Constants.SCREEN_HEIGHT/2);
        this.backGround = mAssets.get("line_paper_bg.jpg", Texture.class);
        this.mainTexture = mAssets.get(Turn.getInstance().getWinner()==Turn.PLAYERONE?"player1_win.png":"player2_win.png", Texture.class);
    }

    @Override
    protected void handleInput() {
        if(Gdx.input.justTouched()){
            ScreenManager.getInstance().push(new WelcomeScreen());
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(
                backGround,
                0,
                0,
                camera.viewportWidth,
                camera.viewportHeight
        );
        batch.draw(
                mainTexture,
                0,0,
                camera.viewportWidth,
                camera.viewportHeight
        );
        batch.end();
    }

    @Override
    public void dispose() {
//        backGround.dispose();
//        mainTexture.dispose();
    }
}
