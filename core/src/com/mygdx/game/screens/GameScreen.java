package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.Constants;
import com.mygdx.game.MainGame;
import com.mygdx.game.models.Arrow;
import com.mygdx.game.models.Cell;
import com.mygdx.game.models.Rock;
import com.mygdx.game.models.Turn;

/**
 * Created by kienvanba on 5/8/17.
 */

public class GameScreen extends Screen {
    private static final int LEFT = -1;
    private static final int RIGHT = 1;
    private int muon = 0;

    private Texture board, background;
    private Vector3 touchPosition;
    private ShapeRenderer shapeRenderer;
    private Rectangle touchableArea;
    private Rectangle sound;
    private Rectangle exit;
    private Texture soundTexture, exitTexture;

    private int directionSelected;
    private int currentSelected;
    private int positionMovement;
    private Cell[] cells = new Cell[12];
    private Cell currentHand;
    private Cell player1;
    private Cell player2;
    private Cell currentPlayer;

    private Arrow leftArrow, rightArrow;
    private BitmapFont font;
    private boolean isPlaying;
    private boolean win;
    private boolean gameStop;

    public GameScreen() {
        camera.setToOrtho(false, Constants.SCREEN_WIDTH /2, Constants.SCREEN_HEIGHT /2);
        board = mAssets.get("Board.png", Texture.class);
        background = mAssets.get("line_paper_bg2.jpg", Texture.class);
        soundTexture = mAssets.get(MainGame.music.getVolume()>0?"speaker.png":"mute.png", Texture.class);
        exitTexture = mAssets.get("exit-door.png", Texture.class);
        shapeRenderer = new ShapeRenderer();

        touchableArea = new Rectangle(
                camera.position.x - 130, camera.position.y - 45,
                250, 100
        );
        exit = new Rectangle(
                0,0,30,30
        );
        sound = new Rectangle(
                camera.viewportWidth-50,
                camera.viewportHeight-50,
                30,30
        );

        initCells();
        currentSelected = -1;
        font = new BitmapFont();
    }

    private void initCells(){
        for (int i = 0; i <= 6; i++) {
            float x = (camera.position.x - 30 - 150) + i * 50;
            float y = ((i == 0 || i == 6) ? camera.position.y + 5 - 50 : camera.position.y + 5);
            cells[i] = new Cell(
                    new Rectangle(
                            x,
                            y,
                            50,
                            (i == 0 || i == 6) ? 100 : 50
                    ),
                    (i == 0 || i == 6)
            );
            if (i > 0 && i < 6) {
                cells[cells.length - i] = new Cell(
                        new Rectangle(
                                x,
                                y - 53,
                                50,
                                52
                        ),
                        false
                );
            }
        }
        currentHand = new Cell(
                new Rectangle(
                        camera.position.x-30,
                        camera.position.y-115,
                        50,
                        50
                ),
                false
        );
        currentHand.getRocks().clear();
        player1 = new Cell(
                new Rectangle(
                        camera.position.x+50,
                        camera.position.y-115,
                        80,
                        50
                ),
                false
        );
        player2 = new Cell(
                new Rectangle(
                        camera.position.x-145,
                        camera.position.y+65,
                        80,
                        50
                ),
                false
        );
        player2.getRocks().clear();
        player1.getRocks().clear();
        currentPlayer = player1;
//        test();
    }

    private void debug(){
        System.out.println("isPlaying: "+String.valueOf(isPlaying));
        System.out.println("win: "+String.valueOf(win));
        System.out.println("is using: "+ Constants.isUse);
        System.out.println("rock left: "+ Constants.rockLeft);
        System.out.println("currentSelected: "+currentSelected);
        String player = Turn.getInstance().getTurn()==Turn.PLAYERONE?"player1": "player2";
        System.out.println("currentPlayer: "+ player);
        System.out.println("positionMovement: "+positionMovement);
        System.out.println("currentHand isEmpty: " + currentHand.getRocks().isEmpty());
        System.out.println("touch position: "+ touchPosition.toString());
    }

    @Override
    protected void handleInput() {
        if (Gdx.input.justTouched()) {
            float x = Gdx.input.getX();
            float y = Gdx.input.getY();
            touchPosition = camera.unproject(new Vector3(x, y, 0));
            debug();
            if(sound.contains(touchPosition.x, touchPosition.y)){
                if(MainGame.music.getVolume()>0){
                    MainGame.music.setVolume(0f);
                    soundTexture = mAssets.get("mute.png", Texture.class);
                }else{
                    MainGame.music.setVolume(0.3f);
                    soundTexture = mAssets.get("speaker.png", Texture.class);
                }
            }
            if(exit.contains(touchPosition.x, touchPosition.y)){
                setWinner();
            }
            if(!isPlaying) {
                if (leftArrow != null && leftArrow.getTriangle().contains(touchPosition.x, touchPosition.y)) {
                    //TODO: switch turn, move balls, eat
                    if (currentSelected < 6) {
                        directionSelected = LEFT;
                    } else {
                        directionSelected = RIGHT;
                    }
                    isPlaying = true;
                    positionMovement = currentSelected;
                } else if (rightArrow != null && rightArrow.getTriangle().contains(touchPosition.x, touchPosition.y)) {
                    //TODO: switch turn, move balls, eat
                    if (currentSelected < 6) {
                        directionSelected = RIGHT;
                    } else {
                        directionSelected = LEFT;
                    }
                    isPlaying = true;
                    positionMovement = currentSelected;
                }
//            else {
                currentSelected = -1;
                leftArrow = null;
                rightArrow = null;
                if (touchableArea.contains(touchPosition.x, touchPosition.y)) {
                    for (int i = 0; i < cells.length; i++) {
                        if (cells[i].isOn(touchPosition.x, touchPosition.y)) {
                            if (!cells[i].getRocks().isEmpty() &&
                                    ((Turn.getInstance().getTurn() == Turn.PLAYERONE && i > 6) ||
                                            (Turn.getInstance().getTurn() == Turn.PLAYERTWO && i < 6)))
                                currentSelected = i;
                            break;
                        }
                    }
                }
            }
//                }
        }
    }

    /*
            todo: operating movement
     */
    private void processMoving(){
        if(win && !Constants.isUse){
            for(Rock rock: cells[positionMovement%12].getRocks()){
                rock.move(currentPlayer.getRandPosition());
                currentPlayer.getRocks().add(rock);
            }
            Constants.isUse=true;
            Constants.rockLeft = cells[positionMovement%12].getRocks().size();
            cells[positionMovement%12].getRocks().clear();
            int pos = getNextPosition(positionMovement);
            if(cells[pos%12].getRocks().isEmpty() && !cells[getNextPosition(pos)%12].getRocks().isEmpty()
                    && (!cells[getNextPosition(pos) % 12].hasKing() || cells[getNextPosition(pos) % 12].getScore() >= 10)){
                positionMovement = getNextPosition(pos);
            }else{
                win = false;
                switchTurn();
            }
        }
        meetCondition();
        if(isPlaying){
            if(!Constants.isUse){
                if(currentHand.getRocks().isEmpty()){
                    for(Rock rock: cells[positionMovement%12].getRocks()){
                        rock.move(currentHand.getRandPosition());
                        currentHand.getRocks().add(rock);
                    }
                    cells[positionMovement%12].getRocks().clear();
                    Constants.isUse = true;
                    Constants.rockLeft = currentHand.getRocks().size();
                }else{
                    int position = currentHand.getRocks().size()-1;
                    Rock rock = currentHand.getRocks().get(position);
                    positionMovement = getNextPosition(positionMovement);
                    rock.move(cells[positionMovement%12].getRandPosition());
                    currentHand.getRocks().remove(position);
                    cells[positionMovement%12].getRocks().add(rock);
                    Constants.isUse=true;
                    Constants.rockLeft = 1;
                    if(currentHand.getRocks().isEmpty()) {
                        int pos = getNextPosition(positionMovement);
                        if (pos % 12 == 0 || pos % 12 == 6) {
                            switchTurn();
                        } else if(cells[pos % 12].getRocks().isEmpty()) {
                            positionMovement = getNextPosition(pos);
                            if(!cells[positionMovement%12].getRocks().isEmpty()
                                    && (!cells[getNextPosition(pos) % 12].hasKing() || cells[getNextPosition(pos) % 12].getScore() >= 10)) {
                                win = true;
                            }else {
                                switchTurn();
                            }
                        } else {
                            positionMovement = getNextPosition(positionMovement);
                        }
                    }
                }
            }
        }
    }

    private void test(){
        for(int i=1; i<6; i++){
            int amount = cells[i].getRocks().size()-1;
            while(amount>=0){
                Rock rock = cells[i].getRocks().get(amount);
                player1.getRocks().add(rock);
                cells[i].getRocks().remove(amount);
                rock.move(player1.getRandPosition());
                amount--;
            }
        }
        for(int i=7; i<12; i++){
            int amount = cells[i].getRocks().size()-1;
            while(amount>=0){
                Rock rock = cells[i].getRocks().get(amount);
                player2.getRocks().add(rock);
                cells[i].getRocks().remove(amount);
                rock.move(player2.getRandPosition());
                amount--;
            }
        }
        player2.getRocks().clear();
    }

    private void setWinner(){
        gameStop = true;
        if(player1.getScore()-muon>player2.getScore()+muon){
            Turn.getInstance().setWinner(Turn.PLAYERONE);
        }else{
            Turn.getInstance().setWinner(Turn.PLAYERTWO);
        }
        switchTurn();
    }

    private void meetCondition(){
        if(!win && !isPlaying && !gameStop) {
            boolean isEmpty = true;
            for(Cell cell: cells){
                if(!cell.getRocks().isEmpty()){
                    isEmpty = false;
                    break;
                }
            }
            if((isEmpty || (cells[0].getRocks().isEmpty() && cells[6].getRocks().isEmpty())) && !Constants.isUse){
                setWinner();
                return;
            }
            isEmpty = true;
            int startPosition;
            if (Turn.getInstance().getTurn() == Turn.PLAYERONE) {
                startPosition = 7;
            } else {
                startPosition = 1;
            }
            for (int i = startPosition; i < startPosition + 5; i++) {
                if (!cells[i].getRocks().isEmpty() || !currentHand.getRocks().isEmpty()) {
                    isEmpty = false;
                    break;
                }
            }
            if (isEmpty) {
                if (currentPlayer.getRocks().size() < 5) {
//                    setWinner();
//                    return;
                    if (Turn.getInstance().getTurn() == Turn.PLAYERONE){
                        //muon player2
                        muon += 5;
                        for (int i = 0; i<5; i++){
                            player2.getRocks().get(i).move(player1.getRandPosition());
                            player1.getRocks().add(player2.getRocks().get(i));
                            player2.getRocks().remove(i);
                        }
                    } else {
                        //muon player1
                        muon -= 5;
                        for (int i = 0; i<5; i++){
                            player1.getRocks().get(i).move(player2.getRandPosition());
                            player2.getRocks().add(player1.getRocks().get(i));
                            player1.getRocks().remove(i);
                        }
                    }
                }
                for (int i = startPosition; i < startPosition + 5; i++) {
                    for (Rock rock : currentPlayer.getRocks()) {
                        if (!rock.isKing()) {
                            cells[i].getRocks().add(rock);
                            currentPlayer.getRocks().remove(rock);
                            rock.move(cells[i].getRandPosition());
                            break;
                        }
                    }
                }
                Constants.isUse = true;
                Constants.rockLeft = 5;
            }
        }
    }

    private void switchTurn(){
        isPlaying = false;
        Constants.isUse = false;
        Constants.rockLeft = 0;
        currentSelected = -1;
        positionMovement = -1;
        Turn.getInstance().switchTurn();
        if(Turn.getInstance().getTurn()== Turn.PLAYERTWO){
            currentHand.changePosition(new Vector3(camera.position.x-30, camera.position.y+85, 0));
            currentPlayer = player2;
        }else{
            currentHand.changePosition(new Vector3(camera.position.x-30, camera.position.y-115, 0));
            currentPlayer = player1;
        }
    }

    private int getNextPosition(int currentPosition){
        currentPosition+=directionSelected;
        if(currentPosition==-1)
            currentPosition=11;
        return currentPosition;
    }

    private void updateRock(float dt){
        for(Cell cell: cells){
            for(Rock rock: cell.getRocks()){
                rock.update(dt);
            }
        }
        for(Rock rock: currentHand.getRocks()){
            rock.update(dt);
        }
        for(Rock rock: player1.getRocks()){
            rock.update(dt);
        }
        for(Rock rock: player2.getRocks()){
            rock.update(dt);
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
        if(gameStop && !Constants.isUse && currentSelected==-1 && !isPlaying)
            ScreenManager.getInstance().push(new EndScreen());
        if(!gameStop)
            processMoving();

        updateRock(dt);
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        font.setColor(Color.GREEN);
        batch.draw(
                background,
                0,
                0,
                camera.viewportWidth,
                camera.viewportHeight
        );
        batch.draw(
                board,
                camera.position.x - (board.getWidth() / 2),
                camera.position.y - (board.getHeight() / 2),
                board.getWidth(),
                camera.viewportHeight / 2
        );
        batch.draw(
                soundTexture,
                sound.getX(),
                sound.getY(),
                sound.getWidth(),
                sound.getHeight()
        );
        batch.draw(
                exitTexture,
                exit.getX(),
                exit.getY(),
                exit.getWidth(),
                exit.getHeight()
        );
        drawRock(batch);
        batch.end();
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.circle(
                Constants.SCREEN_WIDTH / 2 - 30,
                30,
                20);
        shapeRenderer.circle(
                30,
                Constants.SCREEN_HEIGHT / 2 - 30,
                20);
        shapeRenderer.setColor(1, 1, 1, 1);
        if (com.mygdx.game.models.Turn.getInstance().getTurn() == com.mygdx.game.models.Turn.PLAYERONE) {
            shapeRenderer.setColor(Color.GREEN);
        }
        shapeRenderer.circle(
                Constants.SCREEN_WIDTH / 2 - 30,
                30,
                17.5f);
        if (com.mygdx.game.models.Turn.getInstance().getTurn() == com.mygdx.game.models.Turn.PLAYERONE) {
            shapeRenderer.setColor(Color.WHITE);
        } else {
            shapeRenderer.setColor(Color.GREEN);
        }
        shapeRenderer.circle(
                30,
                Constants.SCREEN_HEIGHT / 2 - 30,
                17.5f
        );
        shapeRenderer.end();
        if (currentSelected != -1) {
            drawSelectedCell(cells[currentSelected]);
            if (currentSelected > 6) {
                leftArrow = new Arrow(
                        new Rectangle(
                                cells[currentSelected].getArea().getX() - 20,
                                cells[currentSelected].getArea().getY() - 30,
                                24,
                                25
                        ),
                        Arrow.Direction.LEFT
                );
                rightArrow = new Arrow(
                        new Rectangle(
                                cells[currentSelected].getArea().getX() + cells[currentSelected].getArea().getWidth() - 4,
                                cells[currentSelected].getArea().getY() - 30,
                                24,
                                25
                        ),
                        Arrow.Direction.RIGHT
                );
            } else {
                leftArrow = new Arrow(
                        new Rectangle(
                                cells[currentSelected].getArea().getX() - 20,
                                cells[currentSelected].getArea().getY() + cells[currentSelected].getArea().getHeight() + 5,
                                24,
                                25
                        ),
                        Arrow.Direction.LEFT
                );
                rightArrow = new Arrow(
                        new Rectangle(
                                cells[currentSelected].getArea().getX() + cells[currentSelected].getArea().getWidth() - 4,
                                cells[currentSelected].getArea().getY() + cells[currentSelected].getArea().getHeight() + 5,
                                24,
                                25
                        ),
                        Arrow.Direction.RIGHT
                );
            }
            drawArrow(leftArrow);
            drawArrow(rightArrow);
        }
        batch.begin();
        drawScore(batch);
        batch.end();
    }


    @Override
    public void dispose() {
//        background.dispose();
//        board.dispose();
//        shapeRenderer.dispose();
//        currentHand.dispose();
//        player1.dispose();
//        player2.dispose();
//        currentPlayer.dispose();
    }

    private void drawSelectedCell(Cell c) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.setColor(1, 0, 0, 0.8f);
        shapeRenderer.circle(
                c.getArea().getX() + 10,
                c.getArea().getY(),
                2.5f
        );//bo tron dau duoi ben trai
        shapeRenderer.rectLine(
                c.getArea().getX() + 10,
                c.getArea().getY(),
                c.getArea().getX(),
                c.getArea().getY(),
                5
        );//gach ngan duoi ben trai
        shapeRenderer.circle(
                c.getArea().getX(),
                c.getArea().getY(),
                2.5f
        );//bo tron goc duoi ben trai
        shapeRenderer.rectLine(
                c.getArea().getX(),
                c.getArea().getY(),
                c.getArea().getX(),
                c.getArea().getY() + c.getArea().getHeight(),
                5
        );//gach doc ben trai
        shapeRenderer.circle(
                c.getArea().getX(),
                c.getArea().getY() + c.getArea().getHeight(),
                2.5f
        );//bo tron goc tren ben trai
        shapeRenderer.rectLine(
                c.getArea().getX(),
                c.getArea().getY() + c.getArea().getHeight(),
                c.getArea().getX() + 10,
                c.getArea().getY() + c.getArea().getHeight(),
                5
        );//gach ngan tren ben trai
        shapeRenderer.circle(
                c.getArea().getX() + 10,
                c.getArea().getY() + c.getArea().getHeight(),
                2.5f
        );//bo tron dau tren ben trai
        shapeRenderer.circle(
                c.getArea().getX() + c.getArea().getWidth() - 10,
                c.getArea().getY() + c.getArea().getHeight(),
                2.5f
        );//bo tron dau tren ben phai
        shapeRenderer.rectLine(
                c.getArea().getX() + c.getArea().getWidth() - 10,
                c.getArea().getY() + c.getArea().getHeight(),
                c.getArea().getX() + c.getArea().getWidth(),
                c.getArea().getY() + c.getArea().getHeight(),
                5
        );//gach ngan tren ben phai
        shapeRenderer.circle(
                c.getArea().getX() + c.getArea().getWidth(),
                c.getArea().getY() + c.getArea().getHeight(),
                2.5f
        );//bo tron goc tren ben phai
        shapeRenderer.rectLine(
                c.getArea().getX() + c.getArea().getWidth(),
                c.getArea().getY() + c.getArea().getHeight(),
                c.getArea().getX() + c.getArea().getWidth(),
                c.getArea().getY(),
                5
        );//gach doc ben phai
        shapeRenderer.circle(
                c.getArea().getX() + c.getArea().getWidth(),
                c.getArea().getY(),
                2.5f
        );//bo tron goc duoi ben phai
        shapeRenderer.rectLine(
                c.getArea().getX() + c.getArea().getWidth(),
                c.getArea().getY(),
                c.getArea().getX() + c.getArea().getWidth() - 10,
                c.getArea().getY(),
                5
        );//gach ngan duoi ben phai
        shapeRenderer.circle(
                c.getArea().getX() + c.getArea().getWidth() - 10,
                c.getArea().getY(),
                2.5f
        );//bo tron dau duoi ben phai
        shapeRenderer.end();
    }

    private void drawArrow(Arrow arrow) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setProjectionMatrix(camera.combined);
        if (arrow.getDirection() == Arrow.Direction.LEFT) {
            shapeRenderer.setColor(Color.BLACK);
            shapeRenderer.circle(
                    arrow.getTriangle().getX() + arrow.getTriangle().getWidth(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight(),
                    2.5f
            );
            shapeRenderer.circle(
                    arrow.getTriangle().getX() + arrow.getTriangle().getWidth(),
                    arrow.getTriangle().getY(),
                    2.5f
            );
            shapeRenderer.circle(
                    arrow.getTriangle().getX(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight() / 2,
                    2.5f
            );
            shapeRenderer.rectLine(
                    arrow.getTriangle().getX() + arrow.getTriangle().getWidth(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight(),
                    arrow.getTriangle().getX() + arrow.getTriangle().getWidth(),
                    arrow.getTriangle().getY(),
                    5
            );
            shapeRenderer.rectLine(
                    arrow.getTriangle().getX() + arrow.getTriangle().getWidth(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight(),
                    arrow.getTriangle().getX(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight() / 2,
                    5
            );
            shapeRenderer.rectLine(
                    arrow.getTriangle().getX() + arrow.getTriangle().getWidth(),
                    arrow.getTriangle().getY(),
                    arrow.getTriangle().getX(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight() / 2,
                    5
            );
            shapeRenderer.setColor(Color.GREEN);
            shapeRenderer.triangle(
                    arrow.getTriangle().getX() + arrow.getTriangle().getWidth(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight(),
                    arrow.getTriangle().getX() + arrow.getTriangle().getWidth(),
                    arrow.getTriangle().getY(),
                    arrow.getTriangle().getX(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight() / 2
            );
        } else {
            shapeRenderer.setColor(Color.BLACK);
            shapeRenderer.circle(
                    arrow.getTriangle().getX(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight(),
                    2.5f
            );
            shapeRenderer.circle(
                    arrow.getTriangle().getX(),
                    arrow.getTriangle().getY(),
                    2.5f
            );
            shapeRenderer.circle(
                    arrow.getTriangle().getX() + arrow.getTriangle().getWidth(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight() / 2,
                    2.5f
            );
            shapeRenderer.rectLine(
                    arrow.getTriangle().getX(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight(),
                    arrow.getTriangle().getX(),
                    arrow.getTriangle().getY(),
                    5
            );
            shapeRenderer.rectLine(
                    arrow.getTriangle().getX(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight(),
                    arrow.getTriangle().getX() + arrow.getTriangle().getWidth(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight() / 2,
                    5
            );
            shapeRenderer.rectLine(
                    arrow.getTriangle().getX(),
                    arrow.getTriangle().getY(),
                    arrow.getTriangle().getX() + arrow.getTriangle().getWidth(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight() / 2,
                    5
            );
            shapeRenderer.setColor(Color.GREEN);
            shapeRenderer.triangle(
                    arrow.getTriangle().getX(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight(),
                    arrow.getTriangle().getX(),
                    arrow.getTriangle().getY(),
                    arrow.getTriangle().getX() + arrow.getTriangle().getWidth(),
                    arrow.getTriangle().getY() + arrow.getTriangle().getHeight() / 2
            );
        }
        shapeRenderer.end();
    }

    private void drawRock(SpriteBatch batch){
        for(Cell cell: cells){
            for(Rock rock: cell.getRocks()){
                batch.draw(
                        rock.getTexture(),
                        rock.getPosition().x,
                        rock.getPosition().y,
                        rock.getSize(),
                        rock.getSize()
                );
            }
        }
        for(Rock rock: currentHand.getRocks()){
            batch.draw(
                    rock.getTexture(),
                    rock.getPosition().x,
                    rock.getPosition().y,
                    rock.getSize(),
                    rock.getSize()
            );
        }
        for(Rock rock: player1.getRocks()){
            batch.draw(
                    rock.getTexture(),
                    rock.getPosition().x,
                    rock.getPosition().y,
                    rock.getSize(),
                    rock.getSize()
            );
        }
        for(Rock rock: player2.getRocks()){
            batch.draw(
                    rock.getTexture(),
                    rock.getPosition().x,
                    rock.getPosition().y,
                    rock.getSize(),
                    rock.getSize()
            );
        }
    }

    private void drawScore(SpriteBatch batch){
        for(Cell cell: cells){
            int score = cell.getScore();
            if(score<10){
                batch.draw(
                        toTexture(score),
                        cell.getArea().getX()+5,
                        cell.getArea().getY(),
                        10,
                        10
                );
            }else{
                if(Turn.getInstance().getTurn()==Turn.PLAYERONE){
                    batch.draw(
                            toTexture(score/10),
                            cell.getArea().getX()+5,
                            cell.getArea().getY(),
                            10,10
                    );
                    batch.draw(
                            toTexture(score%10),
                            cell.getArea().getX()+15,
                            cell.getArea().getY(),
                            10,10
                    );
                }else{
                    batch.draw(
                            toTexture(score%10),
                            cell.getArea().getX()+5,
                            cell.getArea().getY(),
                            10,10
                    );
                    batch.draw(
                            toTexture(score/10),
                            cell.getArea().getX()+15,
                            cell.getArea().getY(),
                            10,10
                    );
                }
            }
        }
        int score = player1.getScore();
        if(score<10){
            batch.draw(
                    toTexture(score),
                    Constants.SCREEN_WIDTH/2-30-10,
                    30-5,
                    10,
                    10
            );
        }else{
            if(Turn.getInstance().getTurn()==Turn.PLAYERONE){
                batch.draw(
                        toTexture(score/10),
                        Constants.SCREEN_WIDTH/2-30-20,
                        30-5,
                        10,
                        10
                );
                batch.draw(
                        toTexture(score%10),
                        Constants.SCREEN_WIDTH/2-30-10,
                        30-5,
                        10,
                        10
                );
            }else{
                batch.draw(
                        toTexture(score%10),
                        Constants.SCREEN_WIDTH/2-30-20,
                        30-5,
                        10,
                        10
                );
                batch.draw(
                        toTexture(score/10),
                        Constants.SCREEN_WIDTH/2-30-10,
                        30-5,
                        10,
                        10
                );
            }
        }

        score = player2.getScore();
        if(score<10){
            batch.draw(
                    toTexture(score),
                    30-10,
                    Constants.SCREEN_HEIGHT/2-30-5,
                    10,
                    10
            );
        }else{
            if(Turn.getInstance().getTurn()==Turn.PLAYERONE){
                batch.draw(
                        toTexture(score/10),
                        30-20,
                        Constants.SCREEN_HEIGHT/2-30-5,
                        10,
                        10
                );
                batch.draw(
                        toTexture(score%10),
                        30-10,
                        Constants.SCREEN_HEIGHT/2-30-5,
                        10,
                        10
                );
            }else{
                batch.draw(
                        toTexture(score%10),
                        30-20,
                        Constants.SCREEN_HEIGHT/2-30-5,
                        10,
                        10
                );
                batch.draw(
                        toTexture(score/10),
                        30-10,
                        Constants.SCREEN_HEIGHT/2-30-5,
                        10,
                        10
                );
            }
        }
    }

    private Texture toTexture(int number){
        switch (number){
            case 0:
                return mAssets.get(
                        Turn.getInstance().getTurn()==Turn.PLAYERONE?
                                "zero.png":"zero_invert.png",
                        Texture.class
                );
            case 1:
                return mAssets.get(
                        Turn.getInstance().getTurn()==Turn.PLAYERONE?
                                "one.png":"one_invert.png",
                        Texture.class
                );
            case 2:
                return mAssets.get(
                        Turn.getInstance().getTurn()==Turn.PLAYERONE?
                                "two.png":"two_invert.png",
                        Texture.class
                );
            case 3:
                return mAssets.get(
                        Turn.getInstance().getTurn()==Turn.PLAYERONE?
                                "three.png":"three_invert.png",
                        Texture.class
                );
            case 4:
                return mAssets.get(
                        Turn.getInstance().getTurn()==Turn.PLAYERONE?
                                "four.png":"four_invert.png",
                        Texture.class
                );
            case 5:
                return mAssets.get(
                        Turn.getInstance().getTurn()==Turn.PLAYERONE?
                                "five.png":"five_invert.png",
                        Texture.class
                );
            case 6:
                return mAssets.get(
                        Turn.getInstance().getTurn()==Turn.PLAYERONE?
                                "six.png":"six_invert.png",
                        Texture.class
                );
            case 7:
                return mAssets.get(
                        Turn.getInstance().getTurn()==Turn.PLAYERONE?
                                "seven.png":"seven_invert.png",
                        Texture.class
                );
            case 8:
                return mAssets.get(
                        Turn.getInstance().getTurn()==Turn.PLAYERONE?
                                "eight.png":"eight_invert.png",
                        Texture.class
                );
            case 9:
                return mAssets.get(
                        Turn.getInstance().getTurn()==Turn.PLAYERONE?
                                "nine.png":"nine_invert.png",
                        Texture.class
                );
            default:
                return mAssets.get(
                        Turn.getInstance().getTurn()==Turn.PLAYERONE?
                                "zero.png":"zero_invert.png",
                        Texture.class
                );
        }
    }
}
