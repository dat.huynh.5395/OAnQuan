package com.mygdx.game.screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

/**
 * Created by kienvanba on 5/8/17.
 */

public class ScreenManager {
    private static ScreenManager manager = new ScreenManager();
    private ScreenManager(){
        mScreens = new Stack<Screen>();
        assetManager = new AssetManager();
    }
    public static ScreenManager getInstance(){return manager;}

    private Stack<Screen> mScreens;
    private AssetManager assetManager;

    public void push(Screen screen){
        if(!mScreens.isEmpty())
            mScreens.pop().dispose();
        mScreens.push(screen);
    }
    public void pop(){
        mScreens.pop().dispose();
    }
    public void update(float dt){
        mScreens.peek().update(dt);
    }
    public void render(SpriteBatch batch){
        mScreens.peek().render(batch);
    }
    public AssetManager getAssetManager(){
        return this.assetManager;
    }
}
